import {Component, OnInit, ViewChild} from '@angular/core';
import {Album} from "../../models/Album";

@Component({
  selector: 'app-albums',
  templateUrl: './albums.component.html',
  styleUrls: ['./albums.component.css'],
})
export class AlbumsComponent implements OnInit {

  albums: Album[] = [];
  currentClasses: {} = {};
  currentStyle: {} = {};
  enableAddNewAlbum: boolean = true;

  newAlbum: Album = {
    title: "",
    artist: "",
    songs: [],
    favorite: "",
    year: 0,
    genre: "",
    units: 0,
    cover: "",
    link: ""
  };

  @ViewChild("newAlbumForm") form: any;

  constructor() { }

  ngOnInit(): void {

    this.albums = [
      {
        title: "Boston",
        artist: "Boston",
        songs: ["More Than a Feeling","Peace of Mind","Foreplay/Long Time",
          "Rock & Roll Band","Smokin'","Hitch a Ride",
          "Something About You","Let Me Take You Home Tonight"],
        favorite: "More Than a Feeling",
        year: 1976,
        genre: "Hard Rock/Progressive Rock",
        units: 42000000,
        cover: "../assets/img/BostonBoston.jpg",
        link: "https://en.wikipedia.org/wiki/Boston_(band)"
      },
      {
        title: "Don't Look Back",
        artist: "Boston",
        songs: ["Don't Look Back","The Journey","It's Easy","A Man I'll Never Be","Feelin' Satisfied","Party","Used to Bad News","Don't Be Afraid"],
        favorite: "Don't Look Back",
        year: 1978,
        genre: "Hard Rock/Progressive Rock",
        units: 4000000,
        cover: "../assets/img/Boston_-_Don't_Look_Back.jpg",
        link: "https://en.wikipedia.org/wiki/Boston_(band)"
      },
      {
        title: "Third Stage",
        artist: "Boston",
        songs: ["Amanda","We're Ready","The Launch: a) Countdown b) Ignition c) Third Stage Separation","Cool the Engines","My Destination","A New World (Instrumental)","To Be a Man",	"I Think I Like It","Can'tcha Say (You Believe in Me)/Still in Love","Hollyann"],
        favorite: "Amanda",
        year: 1986,
        genre: "Hard Rock/Progressive Rock",
        units: 500000,
        cover: "../assets/img/Boston_-_Third_Stage.jpg",
        link: "https://en.wikipedia.org/wiki/Boston_(band)"
      },
      {
        title: "Walk On",
        artist: "Boston",
        songs: ["I Need Your Love","Surrender to Me","Livin' for You",	"Walkin' at Night (Instrumental)","Walk On","Get Organ-ized/Get Reorgan-ized (Instrumental)",	"Walk On (Some More)","What's Your Name","Magdalene","We Can Make It"],
        favorite: "I Need Your Love",
        year: 1994,
        genre: "Hard Rock/Progressive Rock",
        units: 4000,
        cover: "../assets/img/Boston_-_Walk_On.jpg",
        link: "https://en.wikipedia.org/wiki/Boston_(band)"
      },
      {
        title: "Corporate America",
        artist: "Boston",
        songs: ["I Had a Good Time","Stare Out Your Window","Corporate America","With You","Someone","Turn It Off","Cryin'","Didn't Mean to Fall in Love","You Gave Up on Love","Livin' for You (Live)"],
        favorite: "I Had a Good Time",
        year: 2002,
        genre: "Hard Rock/Progressive Rock",
        units: 500000,
        cover: "../assets/img/Boston_-_Corporate_America.jpg",
        link: "https://en.wikipedia.org/wiki/Boston_(band)"
      },
      {
        title: "Life, Love & Hope",
        artist: "Boston",
        songs: ["Heaven on Earth","Didn't Mean to Fall in Love","Last Day of School","Sail Away","Life Love and Hope","If You Were in Love","Someday","Love Got Away","Someone (2.0)","You Gave Up on Love (2.0)","Te Quiero Mia","The Way You Look Tonight","O Canada"],
        favorite: "Heaven on Earth",
        year: 2013,
        genre: "Hard Rock/Progressive Rock",
        units: 6000,
        cover: "../assets/img/Boston_Life,_Love_&_Hope.jpg",
        link: "https://en.wikipedia.org/wiki/Boston_(band)"
      }
      ]

    this.setCurrentClasses();

    this.setCurrentStyle()

  }

  setCurrentClasses() {

    this.currentClasses = {

      "btn-success": this.enableAddNewAlbum

    }
  }

  setCurrentStyle() {

    this.currentStyle = {

      "text-decoration": "underline"

    }

  }

  toggleInfo(album: any) {
    console.log("Toggle info clicked");
    album.hide =!album.hide;
  }

  // addNewAlbum() {
  //
  //   this.albums.unshift(this.newAlbum);
  //
  //   this.newAlbum = {
  //     title: "",
  //     artist: "",
  //     songs: [],
  //     favorite: "",
  //     year: 0,
  //     genre: "",
  //     units: 0,
  //     cover: "",
  //     link: ""
  //   }
  //
  // }

  onSubmit({value, valid} : {value: Album, valid: boolean}) {

    if (!valid) {

      alert("Form is not valid");

    } else {

      console.log(value);

    }

    this.albums.unshift(value);

    this.form.reset();

  }


}
