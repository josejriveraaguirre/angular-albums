export interface Album {
  title: string,
  artist: string,
  songs: string[],
  favorite: string,
  year: number,
  genre: string,
  units: number,
  cover?: string,
  link?: string
}
